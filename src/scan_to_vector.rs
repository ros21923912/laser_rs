use std::{
    env,
    collections::HashMap,
    sync::{Arc,Mutex}};
use anyhow::Result;
use strum::IntoEnumIterator; // 0.17.1
use strum_macros::EnumIter; // 0.17.1
use rclrs::{
    Context,
    Node,
    Publisher,
    RclrsError,
    Subscription,
};
use sensor_msgs::msg::LaserScan;
use geometry_msgs::msg::{PointStamped,Point};
use std_msgs::msg::Header;
use builtin_interfaces::msg::Time;
#[derive(Debug, EnumIter,Eq, Hash, PartialEq,Clone)]
enum REGIONS{
    FrontC, FrontL, LeftR,
    LeftC, LeftL, BackR,
    BackC, BackL, RightR,
    RightC, RightL, FrontR,
}
struct Avoider {
    node: Arc<Node>,
    frame_id: String,
    regions_report: HashMap<REGIONS, Vec<f32>>,
    regions_distances: HashMap<REGIONS, i32>,
    obstacle_dist: f32,
    regional_angle: i32,
    normal_lin_vel: f32,
    trans_lin_vel: f32,
    trans_ang_vel: f32,
}
struct Maxima {
    destination: REGIONS,
    distance: f32,
  }
impl Avoider{
    fn new(node:Arc<Node>) -> Result<Self, RclrsError> {
        Ok(Self {
            node: node.into(),
            frame_id: "camera_link".to_string(),
            regions_report:HashMap::new(),
            regions_distances:HashMap::from([
                (REGIONS::FrontC, 0),
                (REGIONS::FrontL, 1),
                (REGIONS::LeftR, 2),
                (REGIONS::LeftC, 3),
                (REGIONS::LeftL, 4),
                (REGIONS::BackR, 5),
                (REGIONS::BackC, 6),
                (REGIONS::BackL, -5),
                (REGIONS::RightR, -4),
                (REGIONS::RightC, -3),
                (REGIONS::RightL, -2),
                (REGIONS::FrontR, -1),
            ]),
            obstacle_dist:0.5,
            regional_angle:30,
            normal_lin_vel:0.5,
            trans_lin_vel:-0.9,
            trans_ang_vel:1.75,
        })}
    fn identify_regions(self, scan: LaserScan) -> Self {
        Self{
                node:self.node,
                frame_id:scan.header.frame_id,
                regions_report:REGIONS::iter().enumerate().map(
                    |(i, region)| if region == REGIONS::FrontC {
                    (
                        REGIONS::FrontC, 
                        scan.ranges[0..(self.regional_angle/2) as usize]
                        .iter()
                        .chain(scan.ranges[(scan.ranges.len()-1)*((self.regional_angle/2) as usize)..].iter())
                        .copied()
                        .filter(|x| *x <= self.obstacle_dist && *x != f32::INFINITY)
                        .collect::<Vec<f32>>()
                    )
                } else {
                // Only objects at a distance less than or equal to the threshold are considered obstacles
                (
                    region, 
                    scan.ranges[(self.regional_angle as usize)*i..((self.regional_angle as usize)*(i+ 1 as usize))]
                    .iter().filter(|x| **x <= self.obstacle_dist && **x != f32::INFINITY)
                    .copied().collect::<Vec<f32>>()
                )
            }).collect::<HashMap<REGIONS, Vec<f32>>>(),
                regions_distances:self.regions_distances,
                obstacle_dist:self.obstacle_dist,
                regional_angle:self.regional_angle,
                normal_lin_vel:self.normal_lin_vel,
                trans_lin_vel:self.trans_lin_vel,
                trans_ang_vel:self.trans_ang_vel,
            }
    }
    fn clearance_test(&self) -> (bool, f32) {
        let goal = REGIONS::FrontC;
        let mut closest = f32::INFINITY;
        let mut regional_dist: f32=0.0;
        let mut maxima = Maxima {
          destination: REGIONS::BackC,
          distance: f32::MIN, 
        };
        self.regions_report.clone().into_iter().for_each(|(region, distances)| {
          regional_dist = (self.regions_distances[&goal] - self.regions_distances[&region]).abs() as f32;
          let float_max=distances.iter().fold(f32::NEG_INFINITY, |max, &val| max.max(val));
          // If there're no obstacles in that region
          if distances.is_empty() && regional_dist < closest {
            closest = regional_dist;
             Maxima {
              destination: region,
              distance: self.obstacle_dist,
            };
          } 
          // Check if it's the clearest option
          else if float_max > maxima.distance {
            closest = closest;
            maxima = Maxima {
              destination: region,
              distance: float_max.clone(),
            };
          }
        });
    
        // Calculate the cost to the chosen orientation
        regional_dist = (self.regions_distances[&maxima.destination] - self.regions_distances[&goal]) as f32;
    
        // Return whether to act or not, and the angular velocity with the appropriate sign
        let act = closest != 0.0;
        let velocity = (regional_dist / regional_dist.abs()) * self.trans_ang_vel;
        (act, velocity)
      }
    fn avoid(self) -> PointStamped{
        let (act, ang_vel) = self.clearance_test();
        PointStamped {
            header: Header {
                frame_id: self.frame_id.clone(),
                stamp: Time {
                    sec: (self.node.get_clock().now().nsec /10_i64.pow(9)) as i32,
                    nanosec: (self.node.get_clock().now().nsec %10_i64.pow(9)) as u32,
                },
            },
            point: Point{
                x: if act {self.trans_lin_vel.into()} else {self.normal_lin_vel.into()},
                y: if act {ang_vel.into()} else {0.0},
                z: 0.0,
            },
        }
    }
}

fn subscriber_eval(msg: Option<LaserScan>,last_scan_time: Option<LaserScan>) -> Result<Option<LaserScan>,RclrsError> {
    Ok(if last_scan_time.is_none(){
        msg
    } else if last_scan_time.as_ref().expect("Data Lost!").header.stamp <= msg.clone().unwrap().header.stamp {
        msg
    } else {
        last_scan_time
    })
}
struct LaserAngle {
    node: Arc<Node>,
    _publisher: Arc<Publisher<PointStamped>>,
    _subscription:Arc<Subscription<LaserScan>>,
    data: Arc<Mutex<Option<LaserScan>>>
}
impl LaserAngle{
    fn new(context: &Context) -> Result<Self, RclrsError>{
        let node = rclrs::create_node(&context, "LaserAngle").expect("node not buildable");
        let last_scan_time: Arc<Mutex<Option<LaserScan>>> = Arc::new(Mutex::new(None));
        let other_scan: Arc<Mutex<Option<LaserScan>>> = Arc::clone(&last_scan_time);
        let _publisher = node
            .create_publisher::<PointStamped>("ObstaclePoint", rclrs::QOS_PROFILE_DEFAULT)
            .expect("Publisher not buildable");
        let _subscription = node.create_subscription::<LaserScan,_>(
        "/scan",
        rclrs::QOS_PROFILE_DEFAULT,
        move |msg: LaserScan| {
            match subscriber_eval(Some(msg), other_scan.lock().unwrap().clone()) {
              Ok(scan) => {*other_scan.lock().unwrap() = scan;},
              Err(e) => {
                panic!("Error in subscriber: {}", e);
              }
            }
          }).expect("Could not create subscription");
        Ok(Self { node, _publisher,_subscription,data:last_scan_time })
    }
}

fn main() -> Result<(), RclrsError> {
    let laser_angle = Arc::new(LaserAngle::new(&Context::new(env::args()).unwrap()).unwrap());
    let publisher_other_thread = Arc::clone(&laser_angle);
    std::thread::spawn(move || {
        std::iter::repeat(()).for_each(|()| {
            publisher_other_thread.
            _publisher
            .publish(
                Avoider::new(publisher_other_thread.node.clone())
                .unwrap()
                .identify_regions(publisher_other_thread.data.lock().unwrap().clone().unwrap())
                .avoid()
            ).expect("Could not publish");
        });
    });
    let node_ref = &laser_angle.node;
    rclrs::spin(node_ref.clone())
}
